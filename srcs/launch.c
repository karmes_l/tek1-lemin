/*
** launch.c for lem in in /home/metz_a/rendu/CPE_2014_lemin
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun Apr 26 20:27:55 2015 Aurélien Metz
** Last update Sun May  3 22:25:33 2015 huy le
*/

#include "launch.h"

void		lem_in(unsigned int nb_ants, t_list *list_room)
{
  t_ant		*ants;
  t_tree	*tree;
  unsigned int	i;

  if (!(ants = pmalloc(sizeof(t_ant) * (nb_ants))))
    return ;
  i = 0;
  memset(ants, 0, sizeof(t_ant) * (nb_ants));
  while (i < nb_ants)
    {
      ants[i].id = i + 1;
      if ((tree = build(ants, list_room->l_start, NULL, 0)))
	ants[i].path_length = shortest_path(tree, &ants[i], 0, 0);
      i = i + 1;
    }
  aff(ants, nb_ants);
}

int		no_valid_branches(t_branch *branches, unsigned int nb_branches)
{
  unsigned int	i;
  unsigned int	valid;

  i = 0;
  valid = 0;
  if (branches == NULL
      || nb_branches == 0)
    return (1);
  while (i < nb_branches)
    {
      if (branches[i].tree && branches[i].weight != INF)
	valid = valid + 1;
      i = i + 1;
    }
  return (valid ? 0 : 1);
}

t_tree		*build(t_ant *ants, t_room *room,
		       t_room **fr, unsigned int turn)
{
  t_tree	*tree;
  unsigned int	i;
  t_link_room	*link;

  if ((link = init(room, fr, &tree, &i)) == NULL)
    return (NULL);
  while (link)
    {
      if (allowed(link->room, fr))
	{
	  tree->branches[i].weight = 1 + booked(ants, link->room, turn + 1);
	  tree->branches[i].tree = build(ants, link->room, refresh(room, fr),
					 turn + tree->branches[i].weight);
	  if (tree->branches[i].tree == NULL || room->e_next == NULL)
	    tree->branches[i].weight = INF;
	  i = i + 1;
	}
      link = link->e_next;
    }
  if (no_valid_branches(tree->branches, i)
      && room->e_next)
    return (NULL);
  tree->nbr_branches = i;
  free(fr);
  return (tree);
}

t_room		**refresh(t_room *room, t_room **src)
{
  unsigned int	i;
  unsigned int	n;
  t_link_room	*link;
  t_room	**fr;

  if ((fr = pmalloc(sizeof(t_room *)
		    * (tablen(src) + room->link_room->size + 2))) == NULL)
    return (NULL);
  i = 0;
  while (src && src[i])
    {
      fr[i] = src[i];
      i = i + 1;
    }
  n = 0;
  link = room->link_room->l_start;
  while (link)
    {
      fr[i++] = link->room;
      link = link->e_next;
      n = n + 1;
    }
  fr[i++] = room;
  fr[i] = NULL;
  return (fr);
}

t_branch	*init_branches(t_room *room, t_room **fr)
{
  unsigned int	i;
  t_link_room	*link;
  t_branch	*branches;

  i = 0;
  link = room->link_room->l_start;
  while (link)
    {
      i += allowed(link->room, fr);
      link = link->e_next;
    }
  if ((branches = pmalloc(sizeof(t_branch) * (i + 1))) == NULL)
    return (NULL);
  memset(branches, 0, sizeof(t_branch) * (i + 1));
  return (branches);
}
