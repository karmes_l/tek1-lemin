/*
** init_room.c for  in /home/karmes_l/Projets/Prog_Elem/Lem_in/CPE_2014_lemin/parser/srcs
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Apr 13 14:47:36 2015 lionel karmes
** Last update Sun May  3 22:01:29 2015 huy le
*/

#include "my.h"

int		command_next(t_list **list_room, t_start_end *start_end,
			     char position)
{
  char		*buff;
  int		valid;

  while ((buff = get_next_line(0)))
    {
      my_putstr(buff);
      my_putchar('\n');
      buff = epur_str(buff);
      if (buff[0] != '\0')
	{
	  if (!(valid = room_is(buff, list_room, start_end, position)))
	    room_error(buff);
	  free(buff);
	  if (valid == 1)
	    return (position);
	  return (-1);
	}
      free(buff);
    }
  return (-1);
}

int		valid_or_not(char *buff, t_list *list_room,
			     t_list *list_tube, t_start_end *start_end)
{
  static int	line = 1;
  int		valid;

  line++;
  if (buff[0] != '\0')
    {
      if ((valid = init_room_next(&list_room, &list_tube, buff, start_end))
	  == -1)
	{
	  free(buff);
	  return (msg_error_line(line));
	}
      if (valid == START || valid == END)
	++line;
    }
  return (0);
}

void		remove_start_end(t_start_end *start_end)
{
  if (start_end->start)
    {
      if (start_end->start->name)
	free(start_end->start->name);
      remove_list(&(start_end->start->link_room));
    free(start_end->start);
    }
  if (start_end->end)
    {
      if (start_end->end->name)
	free(start_end->end->name);
      remove_list(&(start_end->end->link_room));
    free(start_end->end);
    }
}

int		init_room(t_list **list_room)
{
  char		*buff;
  t_start_end	start_end;
  t_list	*list_tube;

  if (init_list(list_room, &list_tube, &start_end))
    return (-1);
  while ((buff = get_next_line(0)))
    {
      my_putstr(buff);
      my_putchar('\n');
      epur_str(buff);
      if (valid_or_not(buff, (*list_room), list_tube, &start_end) == -1)
	{
	  remove_start_end(&start_end);
	  remove_list(&list_tube);
	  return (-1);
	}
      free(buff);
    }
  if (init_room_end(list_room, &start_end))
    {
      remove_list(&list_tube);
      return (-1);
    }
  return (check_room(list_room, list_tube));
}
