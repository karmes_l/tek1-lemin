/*
** init_list.c for init in /home/le_n/rendu/progElem/CPE_2014_lemin/srcs
** 
** Made by huy le
** Login   <le_n@epitech.net>
** 
** Started on  Sun May  3 20:08:55 2015 huy le
** Last update Sun May  3 20:13:08 2015 huy le
*/

#include "my.h"

int		init_list(t_list **list_room, t_list **list_tube,
			  t_start_end *start_end)
{
  if ((*list_room = new_list()) == NULL)
    return (-1);
  (*list_room)->what_element = ROOM;
  if ((*list_tube = new_list()) == NULL)
    return (-1);
  (*list_tube)->what_element = TUBE;
  start_end->start = NULL;
  start_end->end = NULL;
  return (0);
}

int		init_room_next(t_list **list_room, t_list **list_tube,
			       char *str, t_start_end *start_end)
{
  int		valid;

  valid = command_is(str);
  if (valid == -1)
    return (-1);
  if (valid == START || valid == END)
    return (command_next(list_room, start_end, valid));
  valid = room_is(str, list_room, start_end, 0);
  if (valid != 0)
    return (valid);
  valid = tube_is(str, list_tube);
  if (valid != 0)
    return (valid);
  return (unknown_configuration(str));
}

int		init_room_end(t_list **list_room, t_start_end *start_end)
{
  if (start_end->start == NULL || start_end->end == NULL)
    return (start_end_error(0));
  my_put_in_list_start(list_room, start_end->start);
  my_put_in_list_end(list_room, start_end->end);
  return (0);
}
