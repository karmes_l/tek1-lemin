/*
** shortest_path.c for lemin in /home/metz_a/rendu/Prog elem/CPE_2014_lemin
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun May  3 14:23:35 2015 Aurélien Metz
** Last update Sun May  3 17:09:25 2015 Aurélien Metz
*/

#include "launch.h"

int		shortest_path(t_tree *tree, t_ant *ant,
			      unsigned int old, unsigned int turn)
{
  t_branch	*tmp_weight;

  if (turn != 0)
    add(ant, tree->room, old, turn);
  if (tree->room->e_next == NULL)
    return (0);
  tmp_weight = lightest(tree);
  if (tmp_weight->weight == INF)
    return (0);
  return (tmp_weight->weight
	  + shortest_path(tmp_weight->tree, ant,
			  turn + 1, turn + tmp_weight->weight));
}
