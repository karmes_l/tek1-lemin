/*
** tube_is.c for  in /home/loofat_s/rendu/CPE_2014_lemin/srcs
** 
** Made by Steven Loo Fat
** Login   <loofat_s@epitech.net>
** 
** Started on  Fri Apr 17 12:27:44 2015 Steven Loo Fat
** Last update Sun May  3 22:29:47 2015 huy le
*/

#include "my.h"

int		tube_is_next(char **tmp, t_tube *tube, t_list **list_tube)
{
  if (!(tube->room1 = my_strdup(tmp[0])) || !(tube->room2 = my_strdup(tmp[1])))
    {
     free(tube);
     return (-1);
    }
  if (!(my_strcmp(tube->room1, tube->room2)))
    {
      free(tube->room1);
      free(tube->room2);
      free(tube);
      my_putstr("Error can't link room\n");
      return (-1);
    }
  my_put_in_list_end(list_tube, tube);
  return (1);
}

int		tube_is(char *str, t_list **list_tube)
{
  t_tube	*tube;
  char		**tmp;

  tmp = my_str_to_wordtab(str, '-');
  if (my_strtablen(tmp) != 2)
    {
      free_tab(tmp);
      return (tube_error(str));
    }
  if ((tube = malloc(sizeof(*tube))) == NULL)
    {
      free_tab(tmp);
      my_putstr("Error: malloc in fidding_tube function failed\n");
      return (-1);
    }
  if (tube_is_next(tmp, tube, list_tube) == -1)
    {
      free_tab(tmp);
      return (-1);
    }
  free_tab(tmp);
  return (1);
}
