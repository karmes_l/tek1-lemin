/*
** launch2.c for lemin in /home/metz_a/rendu/Prog elem/CPE_2014_lemin
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun May  3 15:04:14 2015 Aurélien Metz
** Last update Sun May  3 15:14:42 2015 Aurélien Metz
*/

#include "launch.h"

t_link_room	*init(t_room *room, t_room **fr,
		      t_tree **tree, unsigned int *i)
{
  if ((*tree = pmalloc(sizeof(t_tree))) == NULL
      || ((*tree)->branches = init_branches(room, fr)) == NULL)
    return (NULL);
  (*tree)->room = room;
  i[0] = 0;
  return (room->link_room->l_start);
}
