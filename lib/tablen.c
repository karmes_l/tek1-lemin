/*
** tablen.c for lemin in /home/metz_a/rendu/Prog elem/CPE_2014_lemin
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Sun May  3 14:57:58 2015 Aurélien Metz
** Last update Sun May  3 14:58:00 2015 Aurélien Metz
*/

#include "lem_in.h"

int	tablen(t_room **tab)
{
  return (tab && tab[0] ? tablen(tab + 1) + 1 : 0);
}
