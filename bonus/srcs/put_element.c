/*
** put_element.c for  in /home/loofat_s/rendu/CPE_2014_lemin/srcs
** 
** Made by Steven Loo Fat
** Login   <loofat_s@epitech.net>
** 
** Started on  Thu Feb 26 19:55:46 2015 lionel karmes
** Last update Sun May  3 17:25:45 2015 huy le
*/

#include "my.h"

void		put_end_element_move(t_list **list, t_move *element)
{
  t_move	*move_l_end;

  move_l_end = (*list)->l_end;
  element->e_next = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_prev = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      move_l_end->e_next = element;
      element->e_prev = (*list)->l_end;
      (*list)->l_end = element;
    }
  (*list)->size++;
}

void		put_start_element_move(t_list **list, t_move *element)
{
  t_move	*move_l_start;

  move_l_start = (*list)->l_start;
  element->e_prev = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_next = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      move_l_start->e_prev = element;
      element->e_next = (*list)->l_start;
      (*list)->l_start = element;
    }
  (*list)->size++;
}

void		put_end_element_turn(t_list **list, t_turn *element)
{
  t_turn	*turn_l_end;

  turn_l_end = (*list)->l_end;
  element->e_next = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_prev = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      turn_l_end->e_next = element;
      element->e_prev = (*list)->l_end;
      (*list)->l_end = element;
    }
  (*list)->size++;
}

void		put_start_element_turn(t_list **list, t_turn *element)
{
  t_turn	*turn_l_start;

  turn_l_start = (*list)->l_start;
  element->e_prev = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_next = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      turn_l_start->e_prev = element;
      element->e_next = (*list)->l_start;
      (*list)->l_start = element;
    }
  (*list)->size++;
}
