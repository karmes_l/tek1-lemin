/*
** init_list.c for init_list in /home/le_n/rendu/progElem/CPE_2014_lemin/bonus/srcs
** 
** Made by huy le
** Login   <le_n@epitech.net>
** 
** Started on  Sun May  3 18:25:21 2015 huy le
** Last update Sun May  3 18:28:29 2015 huy le
*/

#include "my.h"

int	init_list(t_coord *coord, t_start_end *start_end)
{
  char	*buf;

  if ((buf = get_next_line(0)) == NULL)
    return (message_error_ants());
  coord->ants = my_getnbr(buf);
  free(buf);
  if (coord->ants < 1)
    {
      my_putstrerror(INV_ANT_NBR);
      return (-1);
    }
  if ((coord->list_room = new_list()) == NULL)
    return (-1);
  coord->list_room->what_element = ROOM;
  if ((coord->list_turn = new_list()) == NULL)
    return (-1);
  coord->list_turn->what_element = TURN;
  start_end->start = NULL;
  start_end->end = NULL;
  return (0);
}
