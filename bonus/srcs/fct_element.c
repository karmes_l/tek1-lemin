/*
** fct_element.c for  in /home/karmes_l/Projets/Prog_Elem/Corewar/coreware_asm/asm/srcs
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Mar 31 16:33:23 2015 lionel karmes
** Last update Sun May  3 17:40:27 2015 huy le
*/

#include "my.h"

t_room		*find_element_room(t_list *list, char *name)
{
  int		i;
  t_room	*tmp;

  i = 0;
  tmp = list->l_start;
  while (i < list->size)
    {
      if (name == NULL)
	my_putstr("NAME");
      else if (tmp->name == NULL)
	my_putstr("TMPNAME");
      if (!my_strcmp(name, tmp->name))
	return (tmp);
      tmp = tmp->e_next;
      i++;
    }
  return (NULL);
}

t_link_room	*find_element_link_room(t_list *list, char *name)
{
  int		i;
  t_link_room	*tmp;

  i = 0;
  tmp = list->l_start;
  while (i < list->size)
    {
      if (!my_strcmp(name, tmp->room->name))
	return (tmp);
      tmp = tmp->e_next;
      i++;
    }
  return (NULL);
}
